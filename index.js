const express = require("express");
const array = require("./data");
const app = express();
const port = 8000;

app.get("/users", (req, res) => {
    let age = req.query.age;
    let ageNumber = parseInt(age);
    if(!Number.isInteger(ageNumber)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "age is invalid"
        });
    } else {
        return res.status(200).json({
            status: "Successfully get all user",
            users: array.filter( data => {
                return data.age > ageNumber
            })
        });
    }
});

app.get("/users/:userId", (req, res) => {
    let userId = req.params.userId;
    let userIdNumber = parseInt(userId);
    if(!Number.isInteger(userIdNumber)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "userId is invalid"
        });
    } else {
        return res.status(200).json({
            status: "Successfully get user by id",
            users: array.filter( data => {
                return data.id === userIdNumber
            })
        });
    }
});

app.listen(port, () => {
    console.log("App listening on port: ", port);
});